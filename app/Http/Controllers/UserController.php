<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index($id){
        $arr = [
            'id' => 2,
            'firstname' => 'Panupong',
            'lastname' => 'Tuptimthai',
            'skill' => [
                'thai' => 100,
                'eng' => 50
            ]
            ];
            return view('users.user')->with('obj', $arr);
    }
}
